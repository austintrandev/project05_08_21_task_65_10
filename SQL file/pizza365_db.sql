-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2021 at 06:14 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza365_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `combomenu`
--

CREATE TABLE `combomenu` (
  `id` bigint(20) NOT NULL,
  `duong_kinh` int(11) DEFAULT NULL,
  `kich_co` varchar(255) DEFAULT NULL,
  `salad` int(11) DEFAULT NULL,
  `so_luong_nuoc_uong` int(11) DEFAULT NULL,
  `suon` int(11) DEFAULT NULL,
  `thanh_tien` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `combomenu`
--

INSERT INTO `combomenu` (`id`, `duong_kinh`, `kich_co`, `salad`, `so_luong_nuoc_uong`, `suon`, `thanh_tien`) VALUES
(1, 20, 'S', 200, 2, 2, 150000),
(2, 25, 'M', 300, 3, 4, 200000),
(3, 30, 'L', 500, 4, 8, 250000);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `address`, `email`, `full_name`, `phone`) VALUES
(1, '55 Trần Đình Xu Quận 1 HCM', 'tam_thien@gmail.com', 'Thiên Tâm', '0912345678'),
(2, '23 Nguyễn Đình Chiểu Q1 HCM', 'ha_nguyen@gmail.com', 'Nguyễn Hà', '0122345678'),
(3, '34 Lương Nhữ Học Q5 HCM', 'toan_tran@gmail.com', 'Trần Toàn', '09093456789'),
(4, '111 Lý Thái Tổ Q10 HCM', 'phong_ngo@gmail.com', 'Ngô Phong', '0903987654'),
(5, '545 Nguyễn Huệ Đà Nẵng', 'anh_hoang@gmail.com', 'Hoàng Anh', '0907456789');

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` bigint(20) NOT NULL,
  `don_gia` bigint(20) DEFAULT NULL,
  `ma_nuoc_uong` varchar(255) DEFAULT NULL,
  `ngay_cap_nhat` bigint(20) DEFAULT NULL,
  `ngay_tao` bigint(20) DEFAULT NULL,
  `ten_nuoc_uong` varchar(255) DEFAULT NULL,
  `ghi_chu` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `don_gia`, `ma_nuoc_uong`, `ngay_cap_nhat`, `ngay_tao`, `ten_nuoc_uong`, `ghi_chu`) VALUES
(2, 10000, 'TRATAC', 1615177934000, 1615177934000, 'Trà tắc', NULL),
(3, 15000, 'COCA', 1615177934000, 1615177934000, 'Cocacola', NULL),
(4, 15000, 'PEPSI', 1615177934000, 1615177934000, 'Pepsi', NULL),
(5, 5000, 'LAVIE', 1615177934000, 1615177934000, 'Lavie', NULL),
(6, 40000, 'TRASUA', 1615177934000, 1615177934000, 'Trà sữa trân châu', NULL),
(7, 15000, 'FANTA', 1615177934000, 1615177934000, 'Fanta', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `duong_kinh` int(11) DEFAULT NULL,
  `giam_gia` int(11) DEFAULT NULL,
  `id_loai_nuoc_uong` varchar(255) DEFAULT NULL,
  `id_voucher` varchar(255) DEFAULT NULL,
  `kich_co` varchar(255) DEFAULT NULL,
  `loai_pizza` varchar(255) DEFAULT NULL,
  `loi_nhan` varchar(255) DEFAULT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `salad` int(11) DEFAULT NULL,
  `so_luong_nuoc` int(11) DEFAULT NULL,
  `suon` int(11) DEFAULT NULL,
  `thanh_tien` int(11) DEFAULT NULL,
  `trang_thai` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `duong_kinh`, `giam_gia`, `id_loai_nuoc_uong`, `id_voucher`, `kich_co`, `loai_pizza`, `loi_nhan`, `ngay_cap_nhat`, `ngay_tao`, `order_id`, `salad`, `so_luong_nuoc`, `suon`, `thanh_tien`, `trang_thai`, `customer_id`) VALUES
(1, 20, 10, 'PEPSI', '12354', 'S', 'Bacon', 'đế mỏng', '2021-08-05 06:07:21', '2021-08-05 06:07:21', 'LPxxGtCoF9', 200, 2, 2, 150000, 'open', 1),
(2, 25, 10, 'COCA', '12354', 'M', 'Hawaii', 'cay nhiều', '2021-08-05 06:07:21', '2021-08-05 06:07:21', 'ymybZM7raW', 300, 3, 4, 200000, 'open', 2),
(3, 25, 10, 'TRASUA', '12354', 'M', 'Seafood', 'đế mỏng, nhiều hành tây', '2021-08-05 06:07:21', '2021-08-05 06:07:21', 'RySU1cjnfO', 300, 3, 4, 200000, 'open', 3),
(4, 30, 10, 'TRATAC', '12354', 'L', 'Bacon', 'đế mỏng, nhiều sốt', '2021-08-05 06:07:21', '2021-08-05 06:07:21', 'GQh4Yfhf9B', 500, 4, 8, 250000, 'open', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `address`, `email`, `full_name`, `phone`) VALUES
(1, '23 nguyễn đình chiểu q1 hcm', 'phuoc.nguyen@gmail.com', 'nguyễn phước', '0912345678'),
(2, '345 nguyễn văn trỗi qphú nhuận hcm', 'lan_phong@gmail.com', 'phong lan', '0934987123'),
(3, '211 trần bình trọng q5 hcm', 'duc_trung@gmail.com', 'trung đức', '0123334566'),
(4, '55 nguyễn thị minh khai q1 hcm', 'yen_hoang@gmail.com', 'hoàng yến', '0988233433'),
(5, '499 thống nhất gò vấp hcm', 'linh_hoang@gmail.com', 'hoàng thị linh', '0122688988'),
(6, '63 phạm văn đồng thủ đức hcm', 'van_ngo@gmail.com', 'ngô văn', '0933567890');

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` bigint(20) NOT NULL,
  `ghi_chu` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `ma_voucher` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `ngay_cap_nhat` bigint(20) DEFAULT NULL,
  `ngay_tao` bigint(20) DEFAULT NULL,
  `phan_tram_giam_gia` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `ghi_chu`, `ma_voucher`, `ngay_cap_nhat`, `ngay_tao`, `phan_tram_giam_gia`) VALUES
(1, NULL, '16512', 1614361849000, 1614361849000, '10'),
(2, NULL, '12354', 1614361849000, 1614361849000, '10'),
(3, NULL, '12332', 1614361849000, 1614361849000, '10'),
(4, NULL, '12332', 1614361849000, 1614361849000, '10'),
(5, NULL, '95531', 1614361849000, 1614361849000, '10'),
(6, NULL, '81432', 1614361849000, 1614361849000, '10'),
(7, NULL, '15746', 1614361849000, 1614361849000, '10'),
(8, NULL, '76241', 1614361849000, 1614361849000, '10'),
(9, NULL, '64562', 1614361849000, 1614361849000, '10'),
(10, NULL, '25896', 1614361849000, 1614361849000, '10'),
(11, NULL, '87654', 1614361849000, 1614361849000, '20'),
(12, NULL, '86423', 1614361849000, 1614361849000, '20'),
(13, NULL, '46253', 1614361849000, 1614361849000, '20'),
(14, NULL, '46211', 1614361849000, 1614361849000, '20'),
(15, NULL, '36594', 1614361849000, 1614361849000, '20'),
(16, NULL, '24864', 1614361849000, 1614361849000, '20'),
(17, NULL, '23156', 1614361849000, 1614361849000, '20'),
(18, NULL, '13946', 1614361849000, 1614361849000, '20'),
(19, NULL, '34962', 1614361849000, 1614361849000, '20'),
(20, NULL, '26491', 1614361849000, 1614361849000, '20'),
(21, NULL, '94634', 1614361849000, 1614361849000, '30'),
(22, NULL, '87643', 1614361849000, 1614361849000, '30'),
(23, NULL, '61353', 1614361849000, 1614361849000, '30'),
(24, NULL, '64532', 1614361849000, 1614361849000, '30'),
(25, NULL, '89436', 1614361849000, 1614361849000, '30'),
(26, NULL, '73256', 1614361849000, 1614361849000, '30'),
(27, NULL, '21561', 1614361849000, 1614361849000, '30'),
(28, NULL, '35468', 1614361849000, 1614361849000, '30'),
(29, NULL, '32486', 1614361849000, 1614361849000, '30'),
(30, NULL, '96462', 1614361849000, 1614361849000, '40'),
(31, NULL, '41356', 1614361849000, 1614361849000, '40'),
(32, NULL, '76164', 1614361849000, 1614361849000, '40'),
(33, NULL, '72156', 1614361849000, 1614361849000, '40'),
(34, NULL, '46594', 1614361849000, 1614361849000, '40'),
(35, NULL, '35469', 1614361849000, 1614361849000, '40'),
(36, NULL, '34546', 1614361849000, 1614361849000, '40'),
(37, NULL, '34862', 1614361849000, 1614361849000, '40'),
(38, NULL, '14652', 1614361849000, 1614361849000, '40'),
(39, NULL, '40685', 1614361849000, 1614361849000, '40'),
(40, NULL, '92061', 1614361849000, 1614361849000, '50'),
(41, NULL, '70056', 1614361849000, 1614361849000, '50'),
(42, NULL, '41603', 1614361849000, 1614361849000, '50'),
(43, NULL, '80320', 1614361849000, 1614361849000, '50'),
(44, NULL, '40381', 1614361849000, 1614361849000, '50'),
(45, NULL, '44306', 1614361849000, 1614361849000, '50'),
(46, NULL, '10641', 1614361849000, 1614361849000, '50'),
(47, NULL, '70651', 1614361849000, 1614361849000, '50'),
(48, NULL, '80325', 1614361849000, 1614361849000, '50'),
(49, NULL, '50516', 1614361849000, 1614361849000, '50'),
(50, NULL, '10056', 1614361849000, 1614361849000, '50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `combomenu`
--
ALTER TABLE `combomenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_hmsk25beh6atojvle1xuymjj0` (`order_id`),
  ADD KEY `FKsjfs85qf6vmcurlx43cnc16gy` (`customer_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FKsjfs85qf6vmcurlx43cnc16gy` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
